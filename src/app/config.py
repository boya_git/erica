import os
from dotenv import load_dotenv
from flask import current_app


basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Configuration(object):
    FLASK_ENV = os.environ.get('FLASK_ENV')
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = 'app/static/uploads'


class DevelopmentConfig(Configuration):
    FLASK_DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_TEST_DATABASE_URI')
    SECRET_KEY = os.environ.get('SECRET_KEY')

    ### Flask-Security ###
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT')
    SECURITY_PASSWORD_HASH = os.environ.get('SECURITY_PASSWORD_HASH')

class ProductionConfig(Configuration):
    FLASK_DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SECRET_KEY = os.environ.get('SECRET_KEY')

    ### Flask-Security ###
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT')
    SECURITY_PASSWORD_HASH = os.environ.get('SECURITY_PASSWORD_HASH')
