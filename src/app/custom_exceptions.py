class AvatarImageSizeError(Exception):
    """Exception raised if source image size out of range.

    Attributes:
        message -- message to show error to user
    """
    def __init__(self, message: str ='Size error'):
        self.message = message
        super().__init__(self, message)

    def __str__(self):
        return f'[error]: {self.message}'
