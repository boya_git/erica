from flask import render_template, redirect, url_for, flash

from flask_security import login_required, roles_accepted

from app.public import public
from app.models.notes import SimpleNote


@public.route('/', methods=['GET'])
@login_required
@roles_accepted('visiter','admin')
def index():
    return render_template('public/index.jinja')


@public.route('/notes', methods=['GET'])
@login_required
@roles_accepted('visiter','admin')
def notes():
    notes = SimpleNote.query.all()
    return render_template('public/notes.jinja', 
                            notes=notes)


@public.route('/note/<int:note_id>', methods=['GET'])
@login_required
@roles_accepted('visiter','admin')
def note_view(note_id):
    note = SimpleNote.query.filter_by(id=note_id).first()

    if note.is_published == False:
        flash('Note not available!')
        return redirect(url_for('public.notes'))
        
    return render_template('public/note.jinja',
                            note=note)

