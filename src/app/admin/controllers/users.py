import shutil

from flask import current_app
from flask_security import current_user

from app.extentions import db
from app.messages import StatusMessage
from app.custom_exceptions import AvatarImageSizeError
from app.models.users import User, Role, user_datastore
from app.controllers.images import AvatarImage


def get_all_users() -> list:
    all_users = User.query.order_by(User.id).all()
    return all_users


def get_all_roles() -> list:
    all_roles = Role.query.all()
    return all_roles 


# Добавь защиту!
def create_user(role: str, **user_info: dict) -> None:
    user = user_datastore.create_user(**user_info)

    user_datastore.add_role_to_user(user, role)
    db.session.commit()
    return


# Добавь защиту!
def get_user(user_id: int) -> User:
    user = user_datastore.get_user(user_id)
    return user


# TODO допилить
def update_user_active(user_id: int, attr: dict) -> str:
    if user_id == current_user.id:
        raise ValueError(f'You can\'t deactivate yourself')

    try:
        User.query.filter_by(id=user_id).update(attr)
    except Exeption as e:
        return f'[error] {e}'
    else:
        db.session.commit()
        return f'{attr["active"]}'
        


# Добавь защиту!
def delete_user(user_id: int)  -> None:
    user = get_user(user_id)
    
    # Remove user upload dir
    user_upload_dir = current_app.config['UPLOAD_FOLDER'] + f'/users/{user.username}'
    shutil.rmtree(user_upload_dir)

    user_datastore.delete_user(user)
    db.session.commit()
    return


def update_user_avatar(user: User, image) -> StatusMessage:
    uploads_dir = current_app.config['UPLOAD_FOLDER']

    avatar = AvatarImage(user, image, uploads_dir)
    
    try:
        avatar_webpath = avatar.create()
    except AvatarImageSizeError as e:
        message = StatusMessage('error', str(e))
    else:
        User.update_avatar(user.id, avatar_webpath)
        db.session.commit()
        message = StatusMessage('success', 'Avatar successfully updated')
    
    return message
