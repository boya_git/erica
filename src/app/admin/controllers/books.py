import os
import shutil

from flask import current_app

from app.extentions import db
from app.messages import StatusMessage
from app.models.books import ClassicBook, ClassicPage, BookMark, bookmark_schema, bookmarks_schema

def get_all_books() -> list:
    books = ClassicBook.query.all()
    return books


def create_book(book_info: dict) -> StatusMessage:
    book = ClassicBook(**book_info)

    try:
        db.session.add(book)
    except Exception as e:
        db.session.rollback()
        raise str(e)
    else:
        db.session.commit()

    return book.id


def get_book(book_id: int) -> ClassicBook:
    book = ClassicBook.query.filter_by(id=book_id).first()
    if book is None:
        raise Exception('Book not found!')
    return book


def update_book(book_id:int, book_info: dict):
    ClassicBook.query.filter_by(id=book_id).update(book_info)
    db.session.commit()


def update_book_pages_folder(book_id: int, pages_folder: str):
    ClassicBook.set_book_pages_folder(book_id, pages_folder)
    db.session.commit()


def update_book_pages_count(book_id: int, pages_count: int):
    ClassicBook.set_book_pages_count(book_id, pages_count)
    db.session.commit()


def get_page(book_id: int, page_no: int):
    page_file = f'page_{page_no}.txt'
    try:
        folder = ClassicBook.get_pages_folder(book_id)
        with open(os.path.join(folder, page_file), 'r') as page:
            content = page.read().replace(u'\xa0', u' ')
    except TypeError as te:
        raise te
    except FileNotFoundError as fnfe:
        raise FileNotFoundError(f'Page #{page_no} not found!')
    return content


def get_pages_count(book_id: int):
    try:
        count = ClassicBook.get_pages_count(book_id)
    except TypeError as te:
        raise te
    return count


def delete_book(book_id: int) -> StatusMessage:
    book = get_book(book_id)

    db.session.delete(book)
    db.session.commit()

    book_uploads = current_app.config['UPLOAD_FOLDER'] + f'/books/{book.title}'
    if os.path.exists(book_uploads):
        shutil.rmtree(book_uploads)

    message_body = f'Book \"{book.title}\" deleted.'
    message_status = 'success'

    message = StatusMessage(status=message_status, message=message_body)

    return message


def bookmark_set(user_id: int, book_id: int, page_no: int) -> dict:
    bookmark = BookMark(
        user_id=user_id,
        book_id=book_id,
        page_no=page_no)

    try:
        db.session.add(bookmark)
    except Exception as e:
        db.session.rollback()
        raise e
    else:
        db.session.commit()

    return bookmark_schema.dump(bookmark)


def bookmark_remove(bookmark_id: int) -> bool:
    try:
        BookMark.query.filter_by(id=bookmark_id).delete()
    except Exception as e:
        db.session.rollback()
        raise e
    else:
        db.session.commit()
    
    return True


def bookmarks_get(user_id: int, book_id: int) -> list:
    bookmarks = BookMark.query.filter_by(user_id=user_id, book_id=book_id).all()
    return bookmarks_schema.dump(bookmarks)
