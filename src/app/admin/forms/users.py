from flask_wtf import FlaskForm

from wtforms import BooleanField, StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, Email, Length, EqualTo


class CreateUser(FlaskForm):
    username = StringField('Username',
        validators=[DataRequired(),
                    Length(min=2, max=64)])
    email = StringField('Email',
        validators=[DataRequired(),
                    Email()])
    password = StringField('New Password',
        validators=[DataRequired(),
                    Length(min=12, max=255),
                    EqualTo('confirm_password', message='Both password fields must be equal!')])
    confirm_password = StringField('Confirm Password',
        validators=[DataRequired(),
                    Length(min=12, max=255)])
    role = SelectField('Select user role')
    active = BooleanField('Is Active?')
    submit = SubmitField('Save')
