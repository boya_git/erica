from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length


class SimpleNoteForm(FlaskForm):
    title = StringField('Title',
        validators=[Length(min=0, max=128)])
    is_published = BooleanField('Is Published?')
    body = TextAreaField('Note',
        validators=[DataRequired()])
    author = StringField('Author',
        validators=[Length(min=0, max=128)])
    submit = SubmitField('Save')