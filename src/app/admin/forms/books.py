from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed

from wtforms import (BooleanField, 
                     TextAreaField, 
                     StringField, 
                     IntegerField, 
                     SubmitField, 
                     SelectField)

from wtforms.validators import DataRequired, Length


class BookCreateForm(FlaskForm):
    title = StringField('Title',
        validators=[DataRequired(),
                    Length(min=0, max=128)])
    annotation = TextAreaField('Annotation (Not necessary)')
    author = StringField('Author',
        validators=[DataRequired(),
                    Length(min=0, max=128)])
    is_published = BooleanField('Is Published?')
    submit = SubmitField('Save')


class BookChangeForm(FlaskForm):
    title = StringField('Title',
        validators=[DataRequired(),
                    Length(min=0, max=128)])
    annotation = TextAreaField('Annotation')
    author = StringField('Author',
        validators=[DataRequired(),
                    Length(min=0, max=128)])
    is_published = BooleanField('Is Published?')
    submit = SubmitField('Save')


class PageCreateForm(FlaskForm):
    content = TextAreaField('Content')
    submit = SubmitField('Save')


class BookUploadForm(FlaskForm):
    book_file = FileField(validators=[FileRequired(),
                          FileAllowed(['txt'], 'TXT files only!')])
    submit = SubmitField('Upload')
