from flask import render_template

from flask_security import login_required, roles_required

from datetime import datetime
from zoneinfo import ZoneInfo

from app.admin import admin


@admin.route('/', methods=['GET'])
@login_required
@roles_required('admin')
def index():
    now = datetime.now(ZoneInfo("Europe/Minsk"))
    current_time = now.strftime("%H:%M:%S")

    return render_template('admin/index.jinja', current_time=current_time)
