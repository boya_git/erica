from flask import render_template, request, redirect, url_for

from flask_security import login_required, roles_required

from app.extentions import db
from app.models.notes import SimpleNote

from app.admin import admin
from app.admin.forms.notes import SimpleNoteForm


@admin.route('/notes', methods=['GET'])
@login_required
@roles_required('admin')
def notes():
    return render_template('admin/notes/notes.jinja', notes=SimpleNote.query.all())


@admin.route('/notes/add', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def note_add():
    form = SimpleNoteForm()

    if form.validate_on_submit():
        form_data = {
            'title' : form.title.data,
            'is_published' : form.is_published.data,
            'body' : form.body.data,
            'author' : form.author.data
        }
        
        db.session.add(SimpleNote(**form_data))
        db.session.commit()
        
        return redirect(url_for('admin.notes'))

    return render_template('admin/notes/note_add.jinja',
                            form=form)


@admin.route('/notes/<int:note_id>/edit', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def note_edit(note_id):
    form = SimpleNoteForm()
    note = SimpleNote.query.filter_by(id=note_id).first()

    if form.validate_on_submit():
        form_data = {
            'title' : form.title.data,
            'is_published' : form.is_published.data,
            'body' : form.body.data,
            'author' : form.author.data
        }

        SimpleNote.query.filter_by(id=note_id).update(form_data)
        db.session.commit()

        return redirect(url_for('admin.notes'))

    form.is_published.data = note.is_published
    form.body.data = note.body

    return render_template('admin/notes/note_edit.jinja',
                            form=form,
                            note=note)


@admin.route('/notes/delete', methods=['POST'])
@login_required
@roles_required('admin')
def note_delete():
    note_id = request.form['noteDelID']

    SimpleNote.query.filter_by(id=note_id).delete()
    db.session.commit()

    return redirect(url_for('admin.notes'))
