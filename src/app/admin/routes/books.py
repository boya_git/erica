from flask import render_template, redirect, url_for, flash, request, jsonify
from flask import current_app

from flask_security import login_required, roles_required, current_user

from app.messages import StatusMessage
from app.controllers.books import BookFileUploader, BookParser

from app.admin import admin
from app.admin.forms.books import BookCreateForm, BookChangeForm, BookUploadForm
from app.admin.controllers.books import (get_all_books, 
                                         create_book,
                                         get_book, 
                                         update_book, 
                                         update_book_pages_folder, 
                                         update_book_pages_count, 
                                         get_page, 
                                         get_pages_count, 
                                         delete_book,
                                         bookmarks_get, 
                                         bookmark_set, 
                                         bookmark_remove)



@admin.route('/books', methods=['GET'])
@login_required
@roles_required('admin')
def books():
    books = get_all_books()
    return render_template('admin/books/books.jinja',
                            books=books)


@admin.route('/books/add', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def book_add():
    book_form = BookCreateForm()

    if book_form.validate_on_submit():
        book_info = {
            'title' : book_form.title.data,
            'is_published' : book_form.is_published.data,
            'annotation' : book_form.annotation.data,
            'author' : book_form.author.data
        }
        
        try:
            book_id = create_book(book_info)
        except Exception as e:
            message = StatusMessage('error', str(e))
        else:
            message = StatusMessage('success', f'The Book {book_info["title"]} created.')
            return redirect(url_for('admin.book', book_id=book_id))
        
        flash(message.message, message.status)
        return redirect(url_for('admin.books'))
        
    return render_template('admin/books/book_add.jinja',
                            form=book_form)


@admin.route('/book/<int:book_id>', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def book(book_id):
    book_upload_form = BookUploadForm()
    book = get_book(book_id)

    if book_upload_form.validate_on_submit():
        book_file = book_upload_form.book_file.data
        book_uploader = BookFileUploader(book_file, 
                                         book.title, 
                                         current_app.config['UPLOAD_FOLDER'])
        try:
            book_filepath = book_uploader.upload()
        except Exception as e:
            flash(str(e), 'error')
        else:
            book_parser = BookParser(book_filepath, 
                                     book.title,
                                     current_app.config['UPLOAD_FOLDER'])
            pages_folder, pages_count = book_parser.split_to_pages()
            update_book_pages_folder(book.id, pages_folder)
            update_book_pages_count(book.id, pages_count)

        return redirect(request.url)

    return render_template('admin/books/book.jinja',
                            book=book,
                            book_upload_form=book_upload_form)


@admin.route('/book/<int:book_id>/edit', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def book_edit(book_id):
    form = BookChangeForm()
    book = get_book(book_id)

    if form.validate_on_submit():
        form_data = {
            'title' : form.title.data,
            'is_published' : form.is_published.data,
            'annotation' : form.annotation.data,
            'author' : form.author.data
        }
        
        update_book(book.id, form_data)
        
        return redirect(url_for('admin.book', book_id=book.id))

    form.annotation.data = book.annotation
    form.is_published.data = book.is_published

    return render_template('admin/books/book_edit.jinja',
                            form=form,
                            book=book)
    

@admin.route('/book/<int:book_id>/reader')
@login_required
@roles_required('admin')
def book_reader(book_id):
    book = get_book(book_id)
    return render_template('admin/books/reader.jinja', 
                            book=book)


@admin.route('/book/<int:book_id>/reader/get-page', methods=['GET'])
@login_required
@roles_required('admin')
def get_page_content(book_id):
    page_no = request.args.get('page')
    content = get_page(book_id, page_no)
    return content


@admin.route('/book/<int:book_id>/reader/get-pages-count', methods=['GET'])
@login_required
@roles_required('admin')
def get_numder_of_pages(book_id):
    pages_number = get_pages_count(book_id)
    return str(pages_number)


@admin.route('/book/<int:book_id>/delete', methods=['GET'])
@login_required
@roles_required('admin')
def book_delete(book_id):
    msg = delete_book(book_id)
    flash(msg.message, msg.status)
    return redirect(url_for('admin.books'))


@admin.route('/book/<int:book_id>/reader/get-bookmarks', methods=['GET'])
@login_required
@roles_required('admin')
def get_bookmark(book_id):
    user_id = current_user.id

    try:
        bookmarks = bookmarks_get(user_id, book_id)
    except Exception as error:
        response = {
            'status': 'error',
            'message': error
        }
    else:
        response = {
            'status': 'success',
            'bookmarks': bookmarks
        }

    return jsonify(response)


@admin.route('/book/<int:book_id>/reader/set-bookmark', methods=['GET'])
@login_required
@roles_required('admin')
def set_bookmark(book_id):
    page_no = request.args.get('page')
    user_id = current_user.id

    try:
        bookmark = bookmark_set(user_id, book_id, page_no)
    except Exception as error:
        response = {
            'status': 'error',
            'message': error
        }
    else:
        response = {
            'status': 'success',
            'bookmark': bookmark
        }
    
    return jsonify(response)


@admin.route('/book/<int:book_id>/reader/remove-bookmark', methods=['GET'])
@login_required
@roles_required('admin')
def remove_bookmark(book_id):
    bookmark_id = request.args.get('bookmark-id')

    try:
        bookmark_remove(bookmark_id)
    except Exception as error:
        response = {
            'status': 'error',
            'message': str(error)
        }
    else:
        response = {
            'status': 'success',
            'message': 'Bookmark successfully deleted.'
        }

    return response