from flask import render_template, redirect, url_for, flash, current_app, request

from flask_security import login_required, roles_required
from flask_security.utils import hash_password

from app.controllers.images import AvatarImage
from app.custom_exceptions import AvatarImageSizeError
from app.messages import StatusMessage

from app.admin import admin
from app.admin.forms.users import CreateUser
from app.admin.forms.images import AvatarForm

from app.admin.controllers.users import (get_all_users, 
                                         get_all_roles, 
                                         create_user, 
                                         get_user, 
                                         delete_user, 
                                         update_user_avatar, 
                                         update_user_active)


@admin.route('/users', methods=['GET'])
@login_required
@roles_required('admin')
def users_list():
    users = get_all_users()
    return render_template('admin/users/users_list.jinja', users=users)


@admin.route('/users/create', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def user_create():
    roles = get_all_roles()

    form = CreateUser()
    form.role.choices = [role.name for role in roles]

    if form.validate_on_submit():
        user_info = {
            'username' : form.username.data,
            'email' : form.email.data,
            'password' : hash_password(form.password.data),
            'active' : form.active.data
        }

        create_user(form.role.data, **user_info)
        return redirect(url_for('admin.users_list'))

    return render_template('admin/users/user_create.jinja', form=form)


@admin.route('/users/profile/<int:user_id>', methods=['GET', 'POST'])
@login_required
@roles_required('admin')
def user_profile(user_id):
    avatar_form = AvatarForm()
    user = get_user(user_id)

    if avatar_form.validate_on_submit():
        image = avatar_form.image.data
        msg = update_user_avatar(user, image)

        flash(msg.message, msg.status)

    return render_template('admin/users/user_profile.jinja', 
                            user=user, 
                            avatar_form=avatar_form)


@admin.route('/users/profile/<int:user_id>/set-attribute')
@login_required
@roles_required('admin')
def user_set_attribute(user_id):
    active_status = (request.args.get('active') == 'true')
    attribute = {'active' : active_status}

    try:
        response = update_user_active(user_id, attribute)
    except ValueError:
        response = f'[error] You cant deactivate yourself'

    return response


@admin.route('/users/profile/<int:user_id>/delete', methods=['GET'])
@login_required
@roles_required('admin')
def user_delete(user_id):
    delete_user(user_id)
    return redirect(url_for('admin.users_list'))
