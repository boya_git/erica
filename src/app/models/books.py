from datetime import datetime

from app.extentions import db, marshmallow


class ClassicBook(db.Model):
    __tablename__ = 'classic_books'
    id = db.Column(db.BigInteger, primary_key=True)
    title = db.Column(db.String(128), unique=True, nullable=False)
    author = db.Column(db.String(128), nullable=False)
    cover = db.Column(db.String(128), default='/assets/img/blank_book_cover.webp')
    annotation = db.Column(db.Text)
    is_published = db.Column(db.Boolean, default=False) 
    date_of_publication = db.Column(db.DateTime)
    pages_count = db.Column(db.Integer, default=0)
    pages = db.relationship('ClassicPage', backref=db.backref('classic_book'))
    pages_folder = db.Column(db.String(128))
    bookmarks = db.relationship('BookMark')

    @classmethod
    def set_book_pages_folder(cls, book_id, pages_folder):
        return cls.query.filter_by(id=book_id).update(dict(pages_folder=pages_folder))

    @classmethod
    def set_book_pages_count(cls, book_id, pages_count):
        return cls.query.filter_by(id=book_id).update(dict(pages_count=pages_count))

    @classmethod
    def get_pages_folder(cls, book_id: int) -> str:
        try:
            folder = cls.query.with_entities(cls.pages_folder).filter_by(
                 id=book_id).first()[0]
        except TypeError as error:
            raise TypeError(f'Book folder with id {book_id} not found')
        return folder

    @classmethod
    def get_pages_count(cls, book_id: int) -> str:
        try:
            count = cls.query.with_entities(cls.pages_count).filter_by(
                 id=book_id).first()[0]
        except TypeError as error:
            raise TypeError(f'Book folder with id {book_id} not found')
        return count

    def __repr__(self):
        return f'<Book ID: {self.id} | Title: {self.title}>'


class ClassicPage(db.Model):
    __tablename__ = 'classic_pages'
    id = db.Column(db.BigInteger, primary_key=True)
    number = db.Column(db.Integer)
    content = db.Column(db.Text, default='-- No content --')
    book_id = db.Column(db.BigInteger, db.ForeignKey('classic_books.id'))

    def __repr__(self):
        return f'Page #{self.number}'


class BookMark(db.Model):
    __tablename__ = 'bookmarks'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    book_id = db.Column(db.Integer, db.ForeignKey('classic_books.id'))
    page_no = db.Column(db.Integer)


class BookMarkSchema(marshmallow.Schema):
    class Meta:
        fields = (
            'id', 
            'user_id', 
            'book_id',
            'page_no'
        )

bookmark_schema = BookMarkSchema()
bookmarks_schema = BookMarkSchema(many=True)


class Story(db.Model):
    __tablename__ = 'storys'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), unique=True, nullable=False)
    author = db.Column(db.String(128), nullable=False, default='ObsCure')
    cover = db.Column(db.String(128), default='/assets/img/blank_book_cover.webp')
    annotation = db.Column(db.Text)
    is_published = db.Column(db.Boolean, default=False)
    