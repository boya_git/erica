from datetime import datetime

from flask_security import UserMixin, RoleMixin
from flask_security import SQLAlchemyUserDatastore

from app.extentions import db


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.BigInteger, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    avatar = db.Column(db.String(128), default='/assets/img/default_avatar.webp')
    roles = db.relationship('Role', secondary='users_roles',
                                    backref=db.backref('users', lazy='dynamic'))
    bookmarks = db.relationship('BookMark')

    @classmethod
    def update_avatar(cls, user_id, avatar_webpath):
        return cls.query.filter_by(id=user_id).update(dict(avatar=avatar_webpath))

    @classmethod
    def get_attr_val(cls, user_id, attr_name):
        attr = getattr(cls, attr_name)
        return cls.query.filter_by(id=user_id).with_entities(attr).first() 

    def __repr__(self):
        return f'Username: {self.username} | Role: {self.role}'


class Role(db.Model, RoleMixin):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)
    description = db.Column(db.String(255)) 


class UsersRoles(db.Model):
    __tablename__ = 'users_roles'
    id = db.Column(db.BigInteger(), primary_key=True)
    user_id = db.Column(db.BigInteger(), db.ForeignKey('users.id'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id'))


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
