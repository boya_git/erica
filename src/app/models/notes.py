from datetime import datetime

from app.extentions import db


class SimpleNote(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    title = db.Column(db.String(128), default='No title')
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    is_published = db.Column(db.Boolean, default=False)
    body = db.Column(db.Text)
    author = db.Column(db.String(128), default='No author')

    def __repr__(self):
        return f'<Post ID: {self.id} | Title: P{self.title}>'
