// Switch user activity status on click

userActivityStatus.addEventListener("click", async (event) => {
    let current_status = event.target.classList.contains('active');
    let new_status = !current_status;
    let url = window.location.pathname + '/set-attribute?active=' + new_status;
    const response = await fetch(url);
    const result = await response.text()
    const user_active_status = result == 'True';

    

    if (result == 'True' ||
        result == 'False'){
        if (user_active_status){
            event.target.classList.add('active');
        }
        else{
            event.target.classList.remove('active');
        }
    }
    else{
        window.FlashMessage.warning(result, {
            progress: true,
            interactive: true,
            timeout: 8000,
            appear_delay: 200,
            container: '.flash-container',
            theme: 'default',
            classes: {
                container: 'flash-container',
                flash: 'flash-message',
                visible: 'is-visible',
                progress: 'flash-progress',
                progress_hidden: 'is-hidden'
            }
          });
    }
});
