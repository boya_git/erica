function view_allert(message) {
    window.FlashMessage.warning(message, {
        progress: true,
        interactive: true,
        timeout: 8000,
        appear_delay: 200,
        container: '.flash-container',
        theme: 'default',
        classes: {
            container: 'flash-container',
            flash: 'flash-message',
            visible: 'is-visible',
            progress: 'flash-progress',
            progress_hidden: 'is-hidden'
        }
      });
}

class BookReader{
    constructor() {
        this.url = window.location.pathname;
        this.current_page = 1;
        this.content_div = document.getElementById('pageContent');
        this.current_page_input = document.getElementById('currentPageNav');
        this.pages_count = document.getElementById('pagesCountNav');
        this.bookmark_ico = document.getElementById('bookMarkButtonIco');
        this.bookmarks_ul = document.getElementById('bookMarksList');
        this.init();
    }

    async init(){
        this.number_of_pages = parseInt(await this.get_number_of_pages());
        this.bookmarks = await this.get_bookmarks();
        this.update_content();
        this.update_nav();
        this.update_bm_dropdown();
    }

    async update_content(){
        this.content_div.innerHTML = await this.get_page_content();
    }

    async update_nav(){
        this.current_page_input.value = this.current_page;
        this.pages_count.innerHTML = this.number_of_pages;

        if (this.is_bookmark()){
            this.bookmark_ico.classList.replace("far", "fas");
        }
        else {
            this.bookmark_ico.classList.replace("fas", "far");
        }
    }

    get_prev_page(){
        if(this.current_page <= 1) {
            console.log('Cant view prev page, because of you at first page');
        }
        else {
            this.current_page -= 1;
            this.update_content();
            this.update_nav();
        }
    }

    get_next_page(){
        if(this.current_page >= this.number_of_pages) {
            console.log('Cant view next page, because of you at last page');
        }
        else {
            this.current_page += 1;
            this.update_content();
            this.update_nav();
        }
    }

    get_user_input_page(){
        let is_num = /^\d+$/.test(this.current_page_input.value);
        let user_value = parseInt(this.current_page_input.value);
        if(!is_num || user_value < 1 || user_value > this.number_of_pages) {
            view_allert('Page must be between 1 and ' + this.number_of_pages);
        }
        else {
            this.current_page = parseInt(this.current_page_input.value);
        }
        this.update_content();
        this.update_nav();
    }

    async get_page_content(){
        const response = await fetch(this.url + '/get-page?page=' + this.current_page);
        const result = await response.text();
        return result;
    }

    async get_number_of_pages(){
        const response = await fetch(this.url + '/get-pages-count');
        const result = await response.text();
        return result;
    }

    async set_bookmark(){
        const bm_id = this.is_bookmark();
        if (bm_id) {
            const response = await fetch(this.url + '/remove-bookmark?bookmark-id=' + bm_id);
            const result = await response.json();
            if (result['status'] === 'success') {
                const el_index = this.bookmarks.findIndex(bookmark => bookmark.id == bm_id);
                delete this.bookmarks[el_index];
                
                // remove empty element after deleting
                this.bookmarks = this.bookmarks.filter(function (el) {
                    return el != null;
                });
            }
            else{
                view_allert(result['message']);
            }
        }
        else {
            const response = await fetch(this.url + '/set-bookmark?page=' + this.current_page);
            const result = await response.json();
            if (result['status'] === 'success') {
                console.log(this.bookmarks);
                console.log(result['bookmark']);
                this.bookmarks.push(result['bookmark']);
                console.log(this.bookmarks);
            }
            else{
                view_allert(result['message']);
            }
        }
        this.update_nav();
        this.update_bm_dropdown();
    }

    async get_bookmarks(){
        const response = await fetch(this.url + '/get-bookmarks')
        const result = await response.json();
        return result['bookmarks'];
    }

    is_bookmark(){
        let id = NaN;
        this.bookmarks.forEach((bm) => {
            if (this.current_page === bm.page_no){
                id = bm.id;
            }
        });
        return id;
    }

    update_bm_dropdown(){
        this.bookmarks_ul.innerHTML = '';
        if (this.bookmarks.length === 0){
            let li = document.createElement("li");
            li.textContent = 'No bookmarks.';
            this.bookmarks_ul.appendChild(li);
        }
        else{
            this.bookmarks.forEach((bm) => {
                let li = document.createElement("li");
                li.textContent = bm.page_no
                this.bookmarks_ul.appendChild(li);
            });
        }
    }
}


book_reader = new BookReader();

prevPage.addEventListener("click", () => {
    book_reader.get_prev_page();
});

nextPage.addEventListener("click", () => {
    book_reader.get_next_page();
});

currentPageNav.addEventListener("keyup", (event) => {
    if(event.key === "Enter"){
        book_reader.get_user_input_page();
    }
});

bookmarkButton.addEventListener("click", () => {
    book_reader.set_bookmark();
});
