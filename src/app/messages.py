from collections import namedtuple

StatusMessage = namedtuple('StatusMessage', ['status', 'message'])