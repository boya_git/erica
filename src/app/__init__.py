from flask import Flask

from app.extentions import db, migrate, security, marshmallow
from app.admin import admin
from app.public import public
from app.models.users import User, Role, user_datastore


def create_app(Configuration):
    app = Flask(__name__)
    app.config.from_object(Configuration)

    db.init_app(app)
    marshmallow.init_app(app)
    migrate.init_app(app, db)

    ### Flask-Security ###
    security.init_app(app, user_datastore)

    app.register_blueprint(public)
    app.register_blueprint(admin, url_prefix='/admin')

    return app

from app.models.notes import SimpleNote
from app.models.books import ClassicBook