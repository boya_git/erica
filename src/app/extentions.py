from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_security import Security


db = SQLAlchemy()
marshmallow = Marshmallow()
migrate = Migrate()
security = Security()
