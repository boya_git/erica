import os

from flask import current_app
from werkzeug.utils import secure_filename

from app.messages import StatusMessage


class FileUploader:
    allowed_extentions = ('',)

    def __init__(self, file, uploads_folder):
        self.file = file
        self.filename = secure_filename(file.filename)
        self.uploads_folder = uploads_folder
        self.filepath = os.path.join(self.uploads_folder, self.filename)

    @classmethod
    def create_folder(cls, path: str):
        if not os.path.exists(path):
            os.makedirs(path)

    def check_ext(self):
        ext = self.filename.rsplit('.', 1)[-1]
        if ext not in self.allowed_extentions:
            raise ValueError(f'[Error]: The file extention must be one of {self.allowed_extentions}')

    def upload(self):
        try:
            self.check_ext()
        except ValueError as error:
            raise error
        else:
            self.create_folder(self.uploads_folder)
            self.file.save(self.filepath)
        return self.filepath
