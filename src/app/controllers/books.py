import os
import textwrap
import shutil

from app.controllers.uploaders import FileUploader


class BookFileUploader(FileUploader):
    allowed_extentions = ('txt',)

    def __init__(self, file, book_name: str, uploads_folder: str):
        super().__init__(file, uploads_folder)
        self.uploads_folder = f'{uploads_folder}/books/{book_name}'
        self.filepath = os.path.join(self.uploads_folder, self.filename)


class BookParser(object):
    file_encoding = 'utf8'
    characters_in_page = 2750
    extra_line = ''
    page_tmp = ''
    wraped_page_tmp = ''
    paragraph_delimiter = '\n'

    def __init__(self, file_path, book_name, uploads_folder):
        self.file_data = open(file_path, encoding=self.file_encoding)
        self.book_folder = os.path.join(uploads_folder, 'books', book_name)
        self.pages_folder = os.path.join(self.book_folder, 'pages')
        self.remove_folder(self.pages_folder)
        self.create_folder(self.pages_folder)

    @staticmethod
    def create_folder(path: str):
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @staticmethod
    def remove_folder(path: str):
        if os.path.exists(path):
            shutil.rmtree(path)
        return path

    def split_to_pages(self):
        pages_counter = 1
        for line in self.file_data.readlines():
            if line == self.paragraph_delimiter:
                continue
            if self.extra_line:
                self.page_tmp += self.extra_line
                self.extra_line = ''
            self.page_tmp += line
            if len(self.page_tmp) >= self.characters_in_page:
                self.extra_line = self.page_tmp[self.characters_in_page:]
                self.page_tmp = self.page_tmp[:self.characters_in_page]

                # if last char is not space symbol, that mean in is cuted purt of word. 
                # Cut that and paste to extra line
                if self.page_tmp[-1] != ' ':
                    self.extra_line = self.page_tmp.rsplit(' ', 1)[-1] + self.extra_line
                    self.page_tmp = self.page_tmp.rsplit(' ', 1)[0]

                for paragraph in self.page_tmp.split(self.paragraph_delimiter):
                    self.wrapped_paragraph = f'<p>{paragraph}</p>'
                    self.wraped_page_tmp += f'{self.wrapped_paragraph}'

                with open(os.path.join(self.pages_folder, 
                                       f'page_{pages_counter}.txt'), 'w') as page_file:
                    page_file.write(self.wraped_page_tmp)
                    pages_counter += 1
                    self.page_tmp = ''
                    self.wraped_page_tmp = ''

        if self.page_tmp:
            for paragraph in self.page_tmp.split(self.paragraph_delimiter):
                self.wrapped_paragraph = f'<p>{paragraph}</p>'
                self.wraped_page_tmp += f'{self.wrapped_paragraph}'
            with open(os.path.join(self.pages_folder, 
                                f'page_{pages_counter}.txt'), 'w') as page_file:
                page_file.write(self.wraped_page_tmp)
            self.page_tmp = ''
            self.wraped_page_tmp = ''

        return (self.pages_folder, pages_counter)