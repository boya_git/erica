import os

from PIL import Image

from werkzeug.utils import secure_filename

from app.custom_exceptions import AvatarImageSizeError
from app.messages import StatusMessage
from app.models.users import User
from app.extentions import db


class CustomImage:
    def __init__(self, image):
        self.image = Image.open(image)
        self.image_filename = secure_filename(image.filename)

    def resize(self, height: int, width: int):
        self.image = self.image.resize((height, width))
    
    def save(self, path: str, filename: str):
        if not os.path.exists(path):
            os.makedirs(path)
        self.image.save(f'{path}/{filename}')


class AvatarImage(CustomImage):
    size_square = 74

    def __init__(self, user: User, image, uploads_folder: str):
        super().__init__(image)
        self.user = user
        self.user_uploads_folder = uploads_folder + f'/users/{self.user.username}'
        self.avatar_webpath = '/uploads' + f'/users/{self.user.username}/{self.image_filename}'
        
        if os.path.exists(self.user_uploads_folder):
            self.old_avatar_path = f'{self.user_uploads_folder}/{self.user.avatar.split("/")[-1]}'
        else:
            self.old_avatar_path = None

    def check_image_size(self):
        if self.image.width != self.image.height:
            raise AvatarImageSizeError('The sides of the image must be equal!')
        elif self.image.width < 74 or self.image.width > 512:
            raise AvatarImageSizeError('The size of the image sides must be between 74 and 512!')

    def remove_old_avatar(self):
        if self.old_avatar_path:
            os.remove(self.old_avatar_path)

    def create(self):
        self.check_image_size()
        super().resize(self.size_square, self.size_square)
        self.remove_old_avatar()
        super().save(self.user_uploads_folder, self.image_filename)
        return self.avatar_webpath
