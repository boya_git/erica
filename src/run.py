import os

from app import create_app
from app.config import DevelopmentConfig, ProductionConfig


if os.environ.get('FLASK_ENV') == 'production':
    app = create_app(ProductionConfig)
else:
    app = create_app(DevelopmentConfig)
