"""empty message

Revision ID: a3bfa5a2fd20
Revises: 0f4ded3f01c1
Create Date: 2021-08-18 13:16:20.137787

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a3bfa5a2fd20'
down_revision = '0f4ded3f01c1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('chapters', sa.Column('annotation', sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('chapters', 'annotation')
    # ### end Alembic commands ###
