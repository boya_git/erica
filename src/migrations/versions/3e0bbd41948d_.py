"""empty message

Revision ID: 3e0bbd41948d
Revises: 81e538b65a51
Create Date: 2021-06-29 23:31:35.429204

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3e0bbd41948d'
down_revision = '81e538b65a51'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('pages', sa.Column('chapter_id', sa.BigInteger(), nullable=True))
    op.create_foreign_key(None, 'pages', 'chapters', ['chapter_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'pages', type_='foreignkey')
    op.drop_column('pages', 'chapter_id')
    # ### end Alembic commands ###
