import click

from flask import current_app
from flask_migrate import migrate, upgrade, downgrade, init, history

from run import app
from app.extentions import db

app.app_context().push()


@click.group()
def cli():
    pass


### Info ###
@cli.group()
def info():
    pass


@info.command()
def app_info():
    current_env = current_app.config['FLASK_ENV']
    debug_mode = current_app.config['DEBUG']
    print(f'Current environment: {current_env}\n'
          f'Current debug mode: {debug_mode}')


### DB actions ###
@cli.group()
def db_manager():
    pass


@db_manager.command()
def do_init():
    init()


@db_manager.command()
def do_migrate():
    migrate()


@db_manager.command()
def do_upgrade():
    upgrade()


@db_manager.command()
def do_downgrade():
    downgrade()


@db_manager.command()
def show_history():
    history()


@db_manager.command()
def create_tables():
    db.create_all()


if __name__ == '__main__':
    cli()
